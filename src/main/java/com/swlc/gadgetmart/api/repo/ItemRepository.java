package com.swlc.gadgetmart.api.repo;

import com.swlc.gadgetmart.api.dto.ItemDTO;

import java.util.ArrayList;

public interface ItemRepository {

    ArrayList<ItemDTO> findItems();
}
