package com.swlc.gadgetmart.api.repo.impl;

import com.swlc.gadgetmart.api.dto.ItemDTO;
import com.swlc.gadgetmart.api.dto.ItemsDTO;
import com.swlc.gadgetmart.api.repo.ItemRepository;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ItemRepositoryImpl implements ItemRepository {


    @Override
    public ArrayList<ItemDTO> findItems() {

        ArrayList<ItemDTO> items = new ArrayList<>();
        RestTemplate restTemplate = new RestTemplate();
        RestTemplate restTemplateForSOAP = new RestTemplate();

        String xmlString = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "            <soapenv:Envelope \n" +
                "            xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" \n" +
                "            xmlns:sch=\"http://localhost:8080/xml/abans/items\"> \n" +
                "            <soapenv:Header/> \n" +
                "            <soapenv:Body> \n" +
                "            <sch:ItemDetailsRequest>  \n" +
                "            </sch:ItemDetailsRequest> \n" +
                "            </soapenv:Body> \n" +
                "            </soapenv:Envelope>";


        //Create a list for the message converters
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        //Add the String Message converter
        messageConverters.add(new StringHttpMessageConverter());
        //Add the message converters to the restTemplate
        restTemplateForSOAP.setMessageConverters(messageConverters);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.TEXT_XML);
        HttpEntity<String> request = new HttpEntity<String>(xmlString, headers);

        try {

            final ResponseEntity<String> response = restTemplateForSOAP.postForEntity("http://18.136.34.169:8080/abans/service/item-details", request, String.class);
            // Getting abans items
            ItemsDTO abansItems = getItemDTOsFromXml(response.getBody(), "Items");

            items.addAll(abansItems.getItems());

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            // Getting singer items
            ItemsDTO singerItems = restTemplate.getForObject(
                    "http://18.136.34.169:8080/singer/items",
                    ItemsDTO.class
            );

            if (singerItems != null) {
                items.addAll(singerItems.getItems());
            } else {
                System.out.println("Singer items not found!!");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            // Getting softlogic items
            ItemsDTO softLogicItems = restTemplate.getForObject(
                    "http://18.136.34.169:8080/softlogic/items",
                    ItemsDTO.class
            );

            if (softLogicItems != null) {
                items.addAll(softLogicItems.getItems());
            } else {
                System.out.println("Softlogic items not found!!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return items;
    }

    public static Document loadXMLString(String response) throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(response));

        return db.parse(is);
    }

    public static ItemsDTO getItemDTOsFromXml(String response, String tagName) throws Exception {
        Document xmlDoc = loadXMLString(response);
        NodeList nodeList = xmlDoc.getElementsByTagName(tagName);

        List<ItemDTO> itemDTOS = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node x = nodeList.item(i);

            ItemDTO itemDTO = new ItemDTO();

            itemDTO.setId(x.getOwnerDocument().getElementsByTagName("id").item(i).getTextContent());
            itemDTO.setName(x.getOwnerDocument().getElementsByTagName("name").item(i).getTextContent());
            itemDTO.setDescription(x.getOwnerDocument().getElementsByTagName("description").item(i).getTextContent());
            itemDTO.setImage(x.getOwnerDocument().getElementsByTagName("image").item(i).getTextContent());
            itemDTO.setPrice(Double.parseDouble(x.getOwnerDocument().getElementsByTagName("price").item(i).getTextContent()));
            itemDTO.setDeliveryCost(Double.parseDouble(x.getOwnerDocument().getElementsByTagName("deliveryCost").item(i).getTextContent()));
            itemDTO.setBrand(x.getOwnerDocument().getElementsByTagName("brand").item(i).getTextContent());
            itemDTO.setCategory(x.getOwnerDocument().getElementsByTagName("category").item(i).getTextContent());
            itemDTO.setDiscount(Integer.parseInt(x.getOwnerDocument().getElementsByTagName("discount").item(i).getTextContent()));
            itemDTO.setShop(x.getOwnerDocument().getElementsByTagName("shop").item(i).getTextContent());
            itemDTO.setWarranty(x.getOwnerDocument().getElementsByTagName("warranty").item(i).getTextContent());
            itemDTO.setSoldOut(Boolean.parseBoolean(x.getOwnerDocument().getElementsByTagName("soldOut").item(i).getTextContent()));

            itemDTOS.add(itemDTO);
        }

        ItemsDTO itemsDTO = new ItemsDTO();
        itemsDTO.setItems(itemDTOS);
        return itemsDTO;
    }

}
