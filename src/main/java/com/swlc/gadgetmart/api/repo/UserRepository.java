package com.swlc.gadgetmart.api.repo;

import com.swlc.gadgetmart.api.dto.UserDTO;

import java.util.List;

public interface UserRepository {

    UserDTO authenticateUser(String username) throws Exception;

    boolean createUser(UserDTO user) throws Exception;

    UserDTO findUser(String userName, String userType) throws Exception;

    boolean updateUser(UserDTO userDTO) throws Exception;

    List<UserDTO> getAllUsers() throws Exception;
}
