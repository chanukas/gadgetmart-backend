package com.swlc.gadgetmart.api.repo;


import com.swlc.gadgetmart.api.dto.OrderDTO;

import java.util.List;

public interface OrderRepository {
    boolean placeOrder(OrderDTO orderDTO) throws Exception;

    boolean updateOrder(OrderDTO orderDTO) throws Exception;

    List<OrderDTO> getAllOrder(String userId) throws Exception;

    List<OrderDTO> getAllOrder() throws Exception;
}
