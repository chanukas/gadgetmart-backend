package com.swlc.gadgetmart.api.dto;

import java.util.List;

public class OrderDTO {
    private int order_id;
    private int user_id;
    private String name;
    private String address;
    private String contact;
    private String status;
    private double totalCost;
    private List<com.swlc.gadgetmart.api.dto.OrderDetail> orderDetail;

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public List<com.swlc.gadgetmart.api.dto.OrderDetail> getOrderDetail() {
        return orderDetail;
    }

    public void setOrderDetail(List<com.swlc.gadgetmart.api.dto.OrderDetail> orderDetail) {
        this.orderDetail = orderDetail;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
