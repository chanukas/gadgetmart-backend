drop
DATABASE gadget_mart;

CREATE
DATABASE gadget_mart;

USE gadget_mart;


CREATE TABLE user_login
(
    user_id  int          NOT NULL AUTO_INCREMENT,
    name     VARCHAR(500) NOT NULL,
    userType VARCHAR(10)  NOT NULL,
    userName VARCHAR(150) NOT NULL,
    password VARCHAR(100) NOT NULL,
    address  VARCHAR(150) NOT NULL,
    contact  VARCHAR(11)  NOT NULL,
    email    VARCHAR(150) NOT NULL,
    PRIMARY KEY (user_id),
    UNIQUE (userName)
)ENGINE=InnoDB;

insert into user_login
values (0, 'Super Admin', 'ADMIN', 'admin', '12345', 'kurunegala', '0756470925', 'admin@admin.com');

CREATE TABLE orders
(
    order_id      int(10) NOT NULL AUTO_INCREMENT,
    user_id       int(10) NOT NULL,
    address       VARCHAR(150) NOT NULL,
    contact       VARCHAR(11)  NOT NULL,
    totalCost     double       NOT NULL,
    status        VARCHAR(11)  NOT NULL DEFAULT 'PENDING',
    CONSTRAINT PRIMARY KEY (order_id),
    CONSTRAINT FOREIGN KEY (user_id) REFERENCES user_login (user_id) on update cascade on delete cascade
) ENGINE=INNODB;

CREATE TABLE order_detail
(
    order_detail_id int(10) NOT NULL AUTO_INCREMENT,
    order_id        int(10) NOT NULL,
    name            VARCHAR(225) NOT NULL,
    description     VARCHAR(225) NOT NULL,
    image           VARCHAR(225) NOT NULL,
    price           DOUBLE       NOT NULL,
    deliveryCost    DOUBLE       NOT NULL,
    brand           VARCHAR(225) NOT NULL,
    category        VARCHAR(225) NOT NULL,
    discount        int(10) NOT NULL,
    shop            VARCHAR(225) NOT NULL,
    soldOut         BOOL         NOT NULL,
    warranty        VARCHAR(225) NOT NULL,
    qty             int (10) NOT NULL,
    CONSTRAINT PRIMARY KEY (order_detail_id),
    CONSTRAINT FOREIGN KEY (order_id) REFERENCES orders (order_id) on update cascade on delete cascade
) ENGINE=INNODB;



