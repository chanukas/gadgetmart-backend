package com.swlc.gadgetmart.api.controller;

import com.swlc.gadgetmart.api.dto.OrderDTO;
import com.swlc.gadgetmart.api.repo.OrderRepository;
import com.swlc.gadgetmart.api.repo.UserRepository;
import com.swlc.gadgetmart.api.util.TokenValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@CrossOrigin
@RequestMapping("/order")
public class OrderController {

    private final OrderRepository orderRepository;

    private final UserRepository userRepository;


    public OrderController(OrderRepository orderRepository, UserRepository userRepository) {
        this.orderRepository = orderRepository;
        this.userRepository = userRepository;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity placeOrder(@RequestHeader("Authorization") String auth, @RequestBody OrderDTO orderDTO) {
        boolean isValid = new TokenValidator(userRepository).validatePublicToken(auth);
        if (!isValid) {
            return new ResponseEntity<>("Unauthorized request", HttpStatus.UNAUTHORIZED);
        }
        try {
            boolean b = orderRepository.placeOrder(orderDTO);
            return new ResponseEntity<>(b, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateOrder(@RequestHeader("Authorization") String auth, @RequestBody OrderDTO orderDTO) {
        boolean isValid = new TokenValidator(userRepository).validatePublicToken(auth);
        if (!isValid) {
            return new ResponseEntity<>("Unauthorized request", HttpStatus.UNAUTHORIZED);
        }
        try {
            boolean b = orderRepository.updateOrder(orderDTO);
            return new ResponseEntity<>(b, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getOrders(@RequestHeader("Authorization") String auth, @RequestParam("userId") String userId) {
        boolean isValid = new TokenValidator(userRepository).validatePublicToken(auth);
        if (!isValid) {
            return new ResponseEntity<>("Unauthorized request", HttpStatus.UNAUTHORIZED);
        }
        try {
            List<OrderDTO> allOrder = orderRepository.getAllOrder(userId);
            return new ResponseEntity<>(allOrder, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping(value = "/all",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllOrders(@RequestHeader("Authorization") String auth) {
        boolean isValid = new TokenValidator(userRepository).validateAdminToken(auth);
        if (!isValid) {
            return new ResponseEntity<>("Unauthorized request", HttpStatus.UNAUTHORIZED);
        }
        try {
            List<OrderDTO> allOrder = orderRepository.getAllOrder();
            return new ResponseEntity<>(allOrder, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
